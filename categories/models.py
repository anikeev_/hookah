from __future__ import unicode_literals

from django.db import models

from mixes.models import Mix


class Category(models.Model):
    name = models.CharField(max_length=64, blank=False, null=True)
    mixes = models.ManyToManyField(Mix)
