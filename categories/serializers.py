from mixes.serializers import MixListSerializer
from rest_framework import serializers

from models import Category


class CategoryListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name',)


class CategoryDetailSerializer(serializers.HyperlinkedModelSerializer):
    mixes = MixListSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('id', 'name', 'mixes',)
