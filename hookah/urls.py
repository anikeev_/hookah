"""hookah URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from categories.views import CategoryList, CategoryDetail
from mixes.views import MixList, MixDetail

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^categories/$', CategoryList.as_view(), name='category-list'),
    url(r'^categories/(?P<pk>[0-9]+)/$', CategoryDetail.as_view(), name='category-detail'),
    url(r'^mixes/$', MixList.as_view(), name='mix-list'),
    url(r'^mixes/(?P<pk>[0-9]+)/$', MixDetail.as_view(), name='mix-detail'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
