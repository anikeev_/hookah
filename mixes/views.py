from rest_framework import generics

from models import Mix
from serializers import MixDetailSerializer, MixListSerializer


class MixList(generics.ListAPIView):
    queryset = Mix.objects.all()
    serializer_class = MixListSerializer


class MixDetail(generics.RetrieveUpdateAPIView):
    queryset = Mix.objects.all()
    serializer_class = MixDetailSerializer
