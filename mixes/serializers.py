from rest_framework import serializers

from models import Mix
from tobaccos.serializers import TobaccoSerializer


class MixListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mix
        fields = ('id', 'name', 'description', 'image_url', 'rating',)


class MixDetailSerializer(serializers.ModelSerializer):
    tobaccos = TobaccoSerializer(many=True, read_only=True)

    class Meta:
        model = Mix
        fields = ('id', 'name', 'description', 'image_url', 'rating', 'tobaccos')
