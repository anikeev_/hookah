from __future__ import unicode_literals

from django.db import models

from tobaccos.models import Tobacco


class Mix(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=64, blank=False)
    description = models.TextField()
    image_url = models.CharField(max_length=255)
    rating = models.IntegerField(default=100)
    tobaccos = models.ManyToManyField(Tobacco)
    hookah = models.BooleanField(default=False)
    vape = models.BooleanField(default=False)
