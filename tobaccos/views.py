from rest_framework import generics

from models import Mix
from serializers import TobaccoSerializer


class TobaccoList(generics.ListAPIView):
    queryset = Mix.objects.all()
    serializer_class = TobaccoSerializer


class TobaccoDetail(generics.RetrieveAPIView):
    queryset = Mix.objects.all()
    serializer_class = TobaccoSerializer
