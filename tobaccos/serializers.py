from rest_framework import serializers

from models import Tobacco


class TobaccoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tobacco
        fields = ('id', 'name',)
