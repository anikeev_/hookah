from __future__ import unicode_literals

from django.db import models


from tobaccos.models import Tobacco


class Order(models.Model):
    client_name = models.TextField(blank=True)
    client_phone = models.TextField(blank=False)
    client_address = models.TextField(blank=True)


class OrderPosition(models.Model):
    order = models.ForeignKey(Order)
    tobacco = models.ForeignKey(Tobacco)
    count = models.IntegerField(default=1)
